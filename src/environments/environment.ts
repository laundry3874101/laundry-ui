export const environment = {
  production: false,
  frappeServerUrl: 'http://laundry.localhost:8000',
  authServerURL: 'http://laundry.localhost:8000',
  authorizationURL:
    'http://laundry.localhost:8000/api/method/frappe.integrations.oauth2.authorize',
  callbackUrl: 'http://localhost:4200/callback',
  clientId: 'a78492d1b4',
  profileURL:
    'http://laundry.localhost:8000/api/method/frappe.integrations.oauth2.openid_profile',
  revocationURL:
    'http://laundry.localhost:8000/api/method/frappe.integrations.oauth2.revoke_token',
  scope: 'all openid',
  tokenURL:
    'http://laundry.localhost:8000/api/method/frappe.integrations.oauth2.get_token',
  isAuthRequiredToRevoke: true,
  appURL: 'http://localhost:4200',
};
