import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.page.html',
  styleUrls: ['./customer-list.page.css'],
})
export class CustomerListPage implements OnInit {
  fields = ['name', 'mobile_no', 'email_id', 'primary_address'];
  linkLabel = 'name';
  constructor() {}

  ngOnInit() {}
}
