import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerListPageRoutingModule } from './customer-list-routing.module';

import { CustomerListPage } from './customer-list.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerListPageRoutingModule,
    SharedModule,
  ],
  declarations: [CustomerListPage],
})
export class CustomerListPageModule {}
