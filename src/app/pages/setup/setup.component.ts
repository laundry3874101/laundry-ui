// import { Component, OnInit } from '@angular/core';
// import { FormControl, FormGroup, Validators } from '@angular/forms';
// import { MatSnackBar } from '@angular/material/snack-bar';
// import { Account, Shop } from '../../shared/interfaces/list-data.interface';

// @Component({
//   selector: 'app-setup',
//   templateUrl: './setup.component.html',
//   styleUrls: ['./setup.component.scss'],
// })
// export class SetupComponent implements OnInit {
//   shopForm: FormGroup;
//   accountForm: FormGroup;
//   shop: Shop;
//   account: Account;

//   constructor(private matSnackBar: MatSnackBar) {}

//   ngOnInit() {
//     this.createForm();
//   }

//   createForm() {
//     this.shopForm = new FormGroup({
//       name: new FormControl(''),
//       city: new FormControl(''),
//     });

//     this.accountForm = new FormGroup({
//       name: new FormControl(''),
//       email: new FormControl('', Validators.email),
//       phone: new FormControl(''),
//     });

//     this.shop = {
//       name: 'name',
//       city: 'city',
//     };

//     this.account = {
//       name: 'name',
//       email: 'email',
//       phone: 'phone',
//     };
//   }

//   saveShop() {
//     this.shop.name = this.shopForm.get('name').value;
//     this.shop.city = this.shopForm.get('city').value;
//     console.log(this.shop.name);
//     console.log(this.shop.city);
//     this.matSnackBar.open('Shop Details Updated', 'Close', {
//       duration: 2000,
//     });
//   }

//   saveAccount() {
//     this.account.name = this.accountForm.get('name').value;
//     this.account.email = this.accountForm.get('email').value;
//     this.account.phone = this.accountForm.get('phone').value;
//     console.log(this.account.name);
//     console.log(this.account.email);
//     console.log(this.account.phone);
//     this.matSnackBar.open('Account Details Updated', 'Close', {
//       duration: 2000,
//     });
//   }
//   resetForm() {}
// }
