import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GET_ALL_CUSTOMERS } from '../../common/url-strings';
import { map, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AUTHORIZATION, BEARER_HEADER_PREFIX } from '../../common/constants';
import { TokenService } from '../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(
    private readonly http: HttpClient,
    private readonly tokenService: TokenService
  ) {}

  getAllCustomers() {
    return this.tokenService.getToken().pipe(
      switchMap((token) => {
        const url = environment.frappeServerUrl + GET_ALL_CUSTOMERS;
        const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };
        return this.http
          .get<{ message: any[] }>(url, {
            headers,
          })
          .pipe(
            map((res) => {
              return res.message;
            })
          );
      })
    );
  }
}
