// import { Location } from '@angular/common';
// import { Component, OnInit } from '@angular/core';
// import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
// import { ProductService } from '../product.service';

// @Component({
//   selector: 'app-add-products',
//   templateUrl: './add-products.component.html',
//   styleUrls: ['./add-products.component.scss'],
// })
// export class AddProductsComponent implements OnInit {
//   addProductsForm: FormGroup;
//   constructor(
//     private location: Location,
//     private formBuilder: FormBuilder,
//     private productService: ProductService
//   ) {}

//   navigateBack() {
//     this.location.back();
//   }

//   createForm() {
//     this.addProductsForm = this.formBuilder.group({
//       products: this.formBuilder.array([]),
//     });
//     this.addProduct();
//   }

//   addProduct() {
//     const prod = <FormArray>this.addProductsForm.controls['products'];
//     prod.push(
//       this.formBuilder.group({
//         product_name: [''],
//         category: [''],
//         tax_group: [''],
//         price: [''],
//       })
//     );
//   }

//   saveProducts() {
//     // console.log(this.addProductsForm.value);
//     this.productService.addProduct(this.addProductsForm.value).subscribe(() => {
//       console.log;
//     });
//   }

//   removeProduct(control, index) {
//     control.removeAt(index);

//     control.remove;
//   }

//   ngOnInit() {
//     this.createForm();
//   }
// }
