// import { Location } from '@angular/common';
// import { Component, OnInit } from '@angular/core';
// import { FormControl, FormGroup, Validators } from '@angular/forms';
// import { MatSnackBar } from '@angular/material/snack-bar';
// import { ActivatedRoute, Router } from '@angular/router';
// import { ProductService } from '../product.service';

// @Component({
//   selector: 'app-product-details',
//   templateUrl: './product-details.component.html',
//   styleUrls: ['./product-details.component.scss'],
// })
// export class ProductDetailsComponent implements OnInit {
//   product: any;
//   productForm: FormGroup;

//   constructor(
//     private route: ActivatedRoute,
//     private router: Router,
//     private location: Location,
//     private service: ProductService,
//     private matSnackBar: MatSnackBar
//   ) {
//     const id = this.route.snapshot.paramMap.get('id');
//     this.service.getProduct(id).subscribe((res) => {
//       this.product = res.data;
//       this.setValues();
//     });
//   }

//   navigateBack() {
//     this.location.back();
//   }

//   ngOnInit(): void {
//     this.createForm();
//   }

//   createForm() {
//     this.productForm = new FormGroup({
//       name: new FormControl(''),
//       category: new FormControl(''),
//       tax_group: new FormControl(''),
//       price: new FormControl(''),
//     });
//   }

//   setValues() {
//     this.productForm.get('name').setValue(this.product?.product_name);
//     this.productForm.get('category').setValue(this.product?.category);
//     this.productForm.get('tax_group').setValue(this.product?.tax_group);
//     this.productForm.get('price').setValue(this.product?.price);
//   }

//   removeProduct() {
//     this.service.deleteProduct(this.product.name).subscribe(() => {
//       this.router.navigate(['product']);
//     });
//   }

//   saveProduct() {
//     this.product.product_name = this.productForm.get('name').value;
//     this.product.category = this.productForm.get('category').value;
//     this.product.tax_group = this.productForm.get('tax_group').value;
//     this.product.price = this.productForm.get('price').value;
//     this.service.updateProduct(this.product).subscribe(() => {
//       this.matSnackBar.open('Product Updated', 'Close', {
//         duration: 2000,
//       });
//     });
//   }
// }
