import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'callback',
    loadChildren: () =>
      import('./callback/callback.module').then((m) => m.CallbackPageModule),
  }, {
    path: 'customer-list',
    loadChildren: () =>
      import('./customer/customer-list/customer-list.module').then(
        (m) => m.CustomerListPageModule
      ),
  },
  {
    path: 'new/customer',
    loadChildren: () =>
      import('./customer/customer-form/customer-form.module').then(
        (m) => m.CustomerFormPageModule
      ),
  },
  {
    path: 'form/customer/:id',
    loadChildren: () =>
      import('./customer/customer-form/customer-form.module').then(
        (m) => m.CustomerFormPageModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
