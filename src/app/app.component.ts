import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { SET_ITEM, StorageService } from './auth/storage/storage.service';
import {
  LOGGED_IN,
  ONE_SEC_DELAY_IN_MS,
  RETRY_ATTEMPTS,
  USER_EMAIL,
  USER_ROLES,
} from './auth/token/constants';
import { TokenService } from './auth/token/token.service';
import {
  delay,
  filter,
  forkJoin,
  retry,
  retryWhen,
  switchMap,
  take,
} from 'rxjs';
import { ProfileService } from './shared/services/profile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'POS';
  loggedIn: boolean;
  fullName: string = '';
  imageURL: string = '';
  role: string[];
  isSkeletonTextVisible = true;

  constructor(
    public readonly router: Router,
    private readonly storageService: StorageService,
    private readonly tokenService: TokenService,
    private readonly profileService: ProfileService
  ) {}

  ngOnInit() {
    this.storageService.getItem(LOGGED_IN).then((loggedIn) => {
      loggedIn === 'true' ? (this.loggedIn = true) : (this.loggedIn = false);
      if (this.loggedIn) {
        this.loadProfile();
      }
    });

    this.storageService.changes.subscribe({
      next: (res) => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          res.value?.value === 'true'
            ? (this.loggedIn = true)
            : (this.loggedIn = false);

          if (!this.loggedIn) {
            this.router.navigate(['/home']).then(() => {});
          }
          if (this.loggedIn) {
            this.loadProfile();
          }
        }
      },
      error: () => {},
    });
  }

  loadProfile() {
    forkJoin({
      token: this.tokenService.getToken(),
      config: this.tokenService.config,
    })
      .pipe(
        filter(() => this.loggedIn),
        switchMap(({ token, config }) => {
          return this.profileService
            .loadProfile(token, config.profileUrl)
            .pipe(retry(3));
        })
      )
      .subscribe({
        next: (profile) => {
          this.storageService.setItem(USER_EMAIL, profile.email);
          this.storageService.setItem(
            USER_ROLES,
            JSON.stringify(profile.roles)
          );
          this.loggedIn = true;
          this.fullName = profile.name;
          this.imageURL = profile.picture;
          this.isSkeletonTextVisible = false;
        },
        error: () => {},
      });
  }

  logIn() {
    this.tokenService.logIn();
  }

  logOut() {
    this.tokenService.logOut();
  }
}
