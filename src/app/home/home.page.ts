import { Component, OnInit } from '@angular/core';
import { TokenService } from '../auth/token/token.service';
import { switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
  LOGGED_IN,
} from '../auth/token/constants';
import { SET_ITEM, StorageService } from '../auth/storage/storage.service';
import { environment } from '../../environments/environment';
// import { SetupComponent } from '../bank-summary/pages/setup/setup.component';
import { MatDialog } from '@angular/material/dialog';
// import { ORGANIZATION } from '../shared/constants/doctypes-string';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  name: string;
  email: string;
  picture: string;
  loggedIn: boolean;
  accessToken: string;
  phone: string;
  organization: string;
  roles: string[];

  constructor(
    private readonly token: TokenService,
    private readonly http: HttpClient,
    private readonly store: StorageService,
    private readonly dialog: MatDialog
  ) {}

  ngOnInit() {
    this.store.getItem(LOGGED_IN).then((loggedIn) => {
      this.loggedIn = loggedIn === 'true';
      if (this.loggedIn) {
        // this.checkIsOnboard();
        this.loadProfile();
      }
    });

    this.store.changes.subscribe({
      next: (res) => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (this.loggedIn) {
            // this.checkIsOnboard();
            this.loadProfile();
          }
        }
      },
      error: () => {},
    });

    this.token.config
      .pipe(
        switchMap((config) => {
          return this.token.getToken().pipe(
            switchMap((token) => {
              this.accessToken = token;
              return this.http.get<any>(config.profileUrl, {
                headers: { authorization: 'Bearer ' + token },
              });
            })
          );
        })
      )
      .subscribe({
        next: (profile) => {
          this.name = profile.name;
          this.email = profile.email;
          this.picture = profile.picture;
        },
        error: () => {},
      });
    // this.setOrganization();
  }

  loadProfile() {
    this.token.config
      .pipe(
        switchMap((config) => {
          return this.token.getToken().pipe(
            switchMap((token) => {
              return this.http.get<any>(config.profileUrl, {
                headers: { authorization: 'Bearer ' + token },
              });
            })
          );
        })
      )
      .subscribe({
        next: (profile) => {
          this.name = profile.name;
          this.email = profile.email;
          this.picture = profile.picture;
          this.phone = profile.phone_number;
        },
        error: (error) => {},
      });
    this.token.loadFrappeProfile().subscribe({
      next: (profile) => {
        this.roles = [profile?.roles[0]];
        // this.roles = profile?.roles;
      },
      error: (error) => {},
    });
  }

  // setOrganization() {
  //   this.store.getItem(ORGANIZATION).then(org => {
  //     this.organization = JSON.parse(org)?.organization;
  //   });
  // }

  // checkIsOnboard() {
  //   return this.token
  //     .getToken()
  //     .pipe(
  //       switchMap(token => {
  //         const url = `${environment.frappeServerUrl}/api/method/my_bank_summary.custom_method.check_is_onboard`;
  //         const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };
  //         return this.http.get(url, { headers });
  //       }),
  //     )
  //     .subscribe({
  //       next: (arg: any) => {
  //         this.store.setItem(ORGANIZATION, JSON.stringify(arg?.message[0]));
  //         this.setOrganization();
  //       },
  //     });
  // }

  // async openDialog() {
  //   const dialogRef = this.dialog.open(SetupComponent, {
  //     disableClose: true,
  //     width: '350px',
  //     autoFocus: false,
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     this.store.setItem(ORGANIZATION, result);
  //     this.setOrganization();
  //   });
  // }
}
