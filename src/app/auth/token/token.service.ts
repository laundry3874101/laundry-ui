import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Capacitor } from '@capacitor/core';
import { Browser } from '@capacitor/browser';
import { SplashScreen } from '@capacitor/splash-screen';
import { map, switchMap } from 'rxjs/operators';
import { of, from, forkJoin, throwError } from 'rxjs';
import * as sjcl from 'sjcl';

import { OAuth2Config } from './credentials-config';
import {
  STATE,
  CODE_VERIFIER,
  REFRESH_TOKEN,
  EXPIRES_IN,
  ACCESS_TOKEN,
  ID_TOKEN,
  LOGGED_IN,
  ONE_HOUR_IN_SECONDS_NUMBER,
  TWENTY_FIVE_MINUTES_IN_SECONDS_NUMBER,
  CONTENT_TYPE,
  APP_X_WWW_FORM_URLENCODED,
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
  TIME_ZONE,
  AT_CREATION_TIME,
  SESSION_EXPIRY_IN_MS,
} from './constants';
import { StorageService } from '../storage/storage.service';
import { environment } from '../../../environments/environment';
import {
  TIMEZONE_ENDPOINT,
  WEB_LOGOUT,
} from '../../shared/constants/endpoints';
import { DEFAULT_TIMEZONE } from '../../shared/constants/app-strings';
import { FRAPPE_OAUTH2_PROFILE } from '../constants/endpoints';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  private iab = Browser;
  config = of({
    authServerUrl: environment.authServerURL,
    authorizationUrl: environment.authorizationURL,
    callbackUrl: environment.callbackUrl,
    clientId: environment.clientId,
    profileUrl: environment.profileURL,
    revocationUrl: environment.revocationURL,
    scope: environment.scope,
    tokenUrl: environment.tokenURL,
    isAuthRequiredToRevoke: environment.isAuthRequiredToRevoke,
  } as OAuth2Config);
  private headers = {
    [CONTENT_TYPE]: APP_X_WWW_FORM_URLENCODED,
  };
  private splash = SplashScreen;

  constructor(private http: HttpClient, private store: StorageService) {}

  loadTimeZone() {
    return forkJoin({
      config: this.config,
      savedTimeZone: from(this.store.getItem(TIME_ZONE)),
    }).pipe(
      switchMap(({ config, savedTimeZone }) => {
        if (savedTimeZone) {
          return of(savedTimeZone);
        }

        return this.http
          .get<any>(environment.frappeServerUrl + TIMEZONE_ENDPOINT)
          .pipe(
            map((res) => {
              const timeZone = res?.message?.time_zone || DEFAULT_TIMEZONE;
              this.store.setItem(TIME_ZONE, timeZone);
              return timeZone;
            })
          );
      })
    );
  }

  logIn() {
    this.generateAuthUrl().subscribe({
      next: (url) => {
        this.iab
          .open({ url, windowName: '_self' })
          .then((success) => {})
          .catch((fail) => {});
      },
    });
  }

  logOut() {
    this.store
      .getItem(ACCESS_TOKEN)
      .then((token) => this.revokeToken(token, true))
      .then(() => this.store.clear())
      .then(() => this.store.setItem(LOGGED_IN, 'false'))
      .then(() => this.config.toPromise())
      .then((config) => {
        location.href = config.authServerUrl + WEB_LOGOUT + environment.appURL;
      })
      .catch((fail) => {});
  }

  processCode(url: string) {
    const urlParts = new URL(url);
    const query = new URLSearchParams(urlParts.searchParams);
    const code = query.get('code') as string;
    if (!code) {
      return;
    }

    const error = query.get('error');
    if (error) {
      return;
    }

    const state = query.get('state') as string;
    forkJoin({
      savedState: from(this.store.getItem(STATE)),
      codeVerifier: from(this.store.getItem(CODE_VERIFIER)),
      config: this.config,
    })
      .pipe(
        switchMap(({ savedState, codeVerifier, config }) => {
          if (savedState !== state) {
            return of({ ErrorInvalidState: true });
          }
          const req: any = {
            grant_type: 'authorization_code',
            code,
            redirect_uri: config.callbackUrl,
            client_id: config.clientId,
            scope: config.scope,
            code_verifier: codeVerifier,
          };

          return this.http.post<any>(
            config.tokenUrl,
            new URLSearchParams(req).toString(),
            {
              headers: this.headers,
            }
          );
        })
      )
      .subscribe({
        next: (response) => {
          console.log(response);
          
          const atCreationTime = new Date();
          const expiresIn = response.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn)
          );

          this.store.setItem(ACCESS_TOKEN, response.access_token);

          this.saveRefreshToken(response.refresh_token);
          this.store.setItem(AT_CREATION_TIME, atCreationTime.toISOString());
          this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
          this.store.setItem(ID_TOKEN, response.id_token);
          this.store.setItem(LOGGED_IN, 'true');

          this.store.removeItem(STATE);
          this.store.removeItem(CODE_VERIFIER);
          this.refreshCordova();
        },
        error: (err) => {
          console.error({
            timestamp: new Date().toISOString(),
            ...err,
          });
        },
      });
  }

  revokeToken(accessToken: string, refresh: boolean = false) {
    forkJoin({
      config: this.config,
      token: from(this.store.getItem(ACCESS_TOKEN)),
    })
      .pipe(
        switchMap(({ config, token }) => {
          if (config.isAuthRequiredToRevoke) {
            this.headers[AUTHORIZATION] = BEARER_HEADER_PREFIX + accessToken;
          }
          return this.http.post(
            config.revocationUrl,
            new URLSearchParams({ token }).toString(),
            {
              headers: { ...this.headers },
            }
          );
        })
      )
      .subscribe({
        next: (success) => {
          if (refresh) {
            this.refreshCordova();
            this.store.setItem(LOGGED_IN, 'false');
          }
        },
        error: (error) => {},
      });
  }

  getToken() {
    return forkJoin({
      expiration: from(this.store.getItem(EXPIRES_IN)),
      accessToken: from(this.store.getItem(ACCESS_TOKEN)),
      atCreateAt: from(this.store.getItem(AT_CREATION_TIME)),
    }).pipe(
      switchMap(({ expiration, accessToken, atCreateAt }) => {
        if (accessToken === 'undefined') {
          this.store.removeItem(ACCESS_TOKEN).then(() => this.logIn());
          return throwError(() => {
            AccessTokenUndefined: true;
          });
        }

        if (!expiration) {
          this.store.removeItem(EXPIRES_IN).then(() => this.logIn());
          return throwError(() => {
            ErrorInvalidExpiration: true;
          });
        }

        const sessionExpiryTime = new Date(atCreateAt);
        sessionExpiryTime.setMilliseconds(
          sessionExpiryTime.getMilliseconds() + SESSION_EXPIRY_IN_MS
        );

        const now = new Date();
        const expirationTime = new Date(expiration);

        // expire 20 min early
        expirationTime.setSeconds(
          expirationTime.getSeconds() - TWENTY_FIVE_MINUTES_IN_SECONDS_NUMBER
        );

        if (sessionExpiryTime < now) {
          this.logOut();
          return throwError(() => {
            ErrorSessionExpired: true;
          });
        }

        if (now < expirationTime) {
          return of(accessToken);
        }
        return this.refreshToken();
      })
    );
  }

  refreshToken() {
    return this.config.pipe(
      switchMap((config) => {
        return from(this.getRefreshToken()).pipe(
          switchMap((refreshToken) => {
            const requestBody = {
              grant_type: 'refresh_token',
              refresh_token: refreshToken,
              redirect_uri: config.callbackUrl,
              client_id: config.clientId,
              scope: config.scope,
            };
            return this.http
              .post<any>(
                config.tokenUrl,
                new URLSearchParams(requestBody).toString(),
                {
                  headers: this.headers,
                }
              )
              .pipe(
                switchMap((bearerToken) => {
                  this.revokeToken(bearerToken.access_token);
                  const expirationTime = new Date();
                  const atCreationTime = new Date();
                  const expiresIn =
                    bearerToken.expires_in || ONE_HOUR_IN_SECONDS_NUMBER;
                  expirationTime.setSeconds(
                    expirationTime.getSeconds() + Number(expiresIn)
                  );
                  this.store.setItem(EXPIRES_IN, expirationTime.toISOString());
                  this.store.setItem(
                    AT_CREATION_TIME,
                    atCreationTime.toISOString()
                  );
                  this.store.setItem(ACCESS_TOKEN, bearerToken.access_token);

                  this.saveRefreshToken(bearerToken.refresh_token);
                  return of(bearerToken.access_token);
                })
              );
          })
        );
      })
    );
  }

  generateAuthUrl() {
    return this.config.pipe(
      switchMap((config) => {
        const state = this.generateRandomString();
        this.store.setItem(STATE, state);

        const codeVerifier = this.generateRandomString();
        this.store.setItem(CODE_VERIFIER, codeVerifier);

        const challenge = sjcl.codec.base64
          .fromBits(sjcl.hash.sha256.hash(codeVerifier))
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/=/g, '');

        let url = config.authorizationUrl;
        url += '?scope=' + encodeURIComponent(config.scope);
        url += '&response_type=code';
        url += '&client_id=' + config.clientId;
        url += '&redirect_uri=' + encodeURIComponent(config.callbackUrl);
        url += '&state=' + state;
        // url += '&code_challenge_method=S256&prompt=select_account';
        url += '&code_challenge_method=S256';
        url += '&code_challenge=' + challenge;

        return of(url);
      })
    );
  }

  generateRandomString(stateLength: number = 32) {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < stateLength; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  refreshCordova() {
    if (Capacitor.getPlatform() !== 'web') {
      const initialUrl = window.location.href;
      this.splash.show({ autoHide: true });
      window.location.href = initialUrl;
    }
  }

  saveRefreshToken(refreshToken: string) {
    this.store
      .setItem(REFRESH_TOKEN, refreshToken)
      .then((success) => {})
      .catch((fail) => {});
  }

  getRefreshToken(): Promise<string> {
    return this.store.getItem(REFRESH_TOKEN);
  }

  loadFrappeProfile() {
    return this.config.pipe(
      switchMap((config) => {
        return this.getToken().pipe(
          switchMap((token) => {
            return this.http.get<any>(
              environment.frappeServerUrl + FRAPPE_OAUTH2_PROFILE,
              {
                headers: { authorization: 'Bearer ' + token },
              }
            );
          })
        );
      })
    );
  }
}
