export interface OAuth2Config {
  clientId?: string;
  profileUrl?: string;
  tokenUrl?: string;
  authServerUrl?: string;
  authorizationUrl?: string;
  revocationUrl?: string;
  scope?: string;
  callbackUrl?: string;
  isAuthRequiredToRevoke?: boolean;
}
