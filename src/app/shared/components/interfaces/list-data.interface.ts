export interface BankAccount {
  bank_account?: string;
  bank_account_holder_id?: string;
  remarks?: string;
  transaction_freeze_date?: string;
  categorization_freeze_date?: string;
  currency_id?: string;
  currency_desc?: string;
  organization?: string;
  last_value_date?: string;
}

export interface BankAccountHolder {
  bank_account_holder?: string;
  pan?: string;
  organization?: string;
  remarks?: string;
}

export interface Party {
  bank_account_holder_id?: string;
  party?: string;
  organization?: string;
  remarks?: string;
}

export interface TransactionType {
  bank_account_holder_id?: string;
  transaction_type?: string;
  organization?: string;
  remarks?: string;
}

export interface Organization {
  organization?: string;
  remarks?: string;
}

export interface FreezeCategorizationDate {
  organization?: string;
  bank_account_id?: string;
  bank_account_holder_id?: string;
  categorization_freeze_date?: string;
}

export interface FreezeTransactionEntryDate {
  organization?: string;
  bank_account_id?: string;
  bank_account_holder_id?: string;
  transaction_freeze_date?: string;
}

export interface TransactionEntry {
  organization?: string;
  bank_account_id?: string;
  bank_account_holder_id?: string;
  currency_id?: string;
  entries?: Entries[];
}

export interface Entries {
  value_date?: Date;
  deposits?: number;
  withdrawals?: number;
  party?: string;
  transaction_type?: string;
  remark?: string;
  reference?: string;
}

export interface Report {
  organization?: string;
  bank_account_holder_id?: string;
  from_date?: string;
  to_date?: string;
  bank_account_reports?: BankAccountReport[];
  summary?: SummaryReport[];
  transaction_summary: TransactionTypeReport[];
  party_summary?: PartyReport[];
}

export interface BankAccountReport {
  bank_account?: string;
  bank_account_id?: string;
  opening_balance_as_per_bank_statement?: number;
  closing_balance_as_per_bank_statement?: number;
}

export interface SummaryReport {
  bank_account_id?: string;
  opening_balance?: number;
  closing_balance?: number;
  bank_account?: string;
  value_date?: string;
  deposits?: string;
  withdrawals?: string;
  transaction_type?: string;
}

export interface TransactionTypeReport {
  bank_account?: string;
  transaction_type?: string;
  deposits?: string;
  withdrawals?: string;
}

export interface PartyReport {
  bank_account?: string;
  party?: string;
  deposits?: string;
  withdrawals?: string;
}

export interface Attachment {
  file_name?: string;
  file_url?: string;
  file_description?: string;
  google_account_name?: string;
}

export interface Account {
  name?: string;
}

export interface GoogleAccount {
  name?: string;
  google_drive_account?: string;
  organization?: string;
}
