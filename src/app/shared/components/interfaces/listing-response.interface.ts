export interface ListResponse {
  docs: unknown[];
  length: number;
  offset?: number;
}
