import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ListingDataSource } from './data-list.datasource';
import { ListingService } from './listing.service';
@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss'],
})
export class DataListComponent implements OnInit {
  @Input() doctype = 'Activity Log';
  @Input() fields = ['name', 'modified', 'owner', 'modified_by'];
  @Input() linkLabel = 'name';
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: ListingDataSource;
  filters: string[][] = [];

  constructor(
    private readonly listingService: ListingService,
    public readonly toastController: ToastController,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.dataSource = new ListingDataSource(
      this.doctype,
      this.fields,
      this.listingService
    );
    this.router.events.subscribe({
      next: () => {
        this.dataSource.loadItems(this.filters);
      },
      error: () => {},
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      buttons: [
        {
          icon: 'close-outline',
          role: 'cancel',
        },
      ],
    });
    toast.present();
  }

  getUpdate(event?) {
    this.dataSource.loadItems(
      this.filters,
      `${this.sort.active || 'modified'} ${this.sort.direction || 'desc'}`,
      event?.pageIndex || this.paginator.pageIndex,
      event?.pageSize || this.paginator.pageSize
    );
  }

  snakeToTitleCase(str: string) {
    if (!str) return;

    return str
      .split('_')
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }

  kebabCase(str: string) {
    if (!str) return;

    return str
      .replace(/([a-z])([A-Z])/g, '$1-$2')
      .replace(/\s+/g, '-')
      .toLowerCase();
  }

  hideCustomName(col) {
    if (col !== 'custom_name') {
      return false;
    } else {
      return true;
    }
  }

  addWildCard(value: string) {
    return '%' + value + '%';
  }
}
