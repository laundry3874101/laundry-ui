/* eslint-disable max-len */
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import {
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
} from '../../../auth/token/constants';
import { TokenService } from '../../../auth/token/token.service';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService
  ) {}

  findModels(
    model: string,
    filters = [],
    sortOrder = 'modified asc',
    pageNumber = 0,
    fields = ['name', 'owner', 'modified', 'modified_by'],
    pageSize = 10
  ) {
    return this.token.getToken().pipe(
      switchMap((token) => {
        const url = `${environment.frappeServerUrl}/api/method/laundry.api.get_list`;
        const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };
        const offset = pageNumber * pageSize;
        const params = new HttpParams()
          .set('doctype', model)
          .set('limit_start', offset.toString())
          .set('limit_page_length', pageSize.toString())
          .set('fields', JSON.stringify(fields))
          .set('filters', JSON.stringify(filters))
          .set('order_by', sortOrder);

        return this.http
          .get<{ message: { docs: any[]; length: number } }>(url, {
            params,
            headers,
          })
          .pipe(
            map((res) => {
              return {
                docs: res.message?.docs,
                offset,
                length: res.message?.length,
              };
            })
          );
      })
    );
  }

  findModelsById(model: string, doctype: string) {
    return this.token.getToken().pipe(
      switchMap((token) => {
        const url = `${environment.frappeServerUrl}/api/resource/${doctype}/${model}`;
        const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };

        return this.http.get<{ data: any }>(url, { headers });
      })
    );
  }

  findModelsById_for_googleaccountlist(model: string, doctype: string) {
    return this.token.getToken().pipe(
      switchMap((token) => {
        const url = `${environment.frappeServerUrl}/api/resource/${doctype}${model}`;
        const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };
        return this.http.get<{ data: any }>(url, { headers });
      })
    );
  }

  // findModelsById_for_accountlist(organization: any) {
  //   return this.token.getToken().pipe(
  //     switchMap(token => {
  //       const url = `${environment.frappeServerUrl}/api/resource/Google Account?fields=["google_drive_account"]&filters=[["organization", "=","${organization}"]]`;
  //       const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };

  //       return this.http.get<{ data: any }>(url, { headers });
  //     }),
  //   );
  // }

  // findModelsById_for_accountlist(doctype: string) {
  //   return this.token.getToken().pipe(
  //     switchMap((token) => {
  //       const url = `${environment.frappeServerUrl}/api/method/laundry.api.get_google_accounts?doctype=${doctype}`;
  //       const headers = { [AUTHORIZATION]: BEARER_HEADER_PREFIX + token };

  //       return this.http.get<{ data: any }>(url, { headers });
  //     })
  //   );
  // }
}
