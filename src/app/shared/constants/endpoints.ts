export const WEB_LOGOUT = '/auth/logout?redirect=';
export const TIMEZONE_ENDPOINT = '/api/method/frappe.client.get_time_zone';
export const DOWNLOAD_PDF =
  '/api/method/frappe.utils.print_format.download_pdf';
