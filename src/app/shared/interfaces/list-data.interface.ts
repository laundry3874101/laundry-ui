export interface Account {
  name: string;
  email: string;
  phone: string;
}

export interface Customer {
  phone: string;
  name: string;
  order_count: string;
  order_value: string;
}

export interface Product {
  name: string;
  options: string;
  category: string;
  tax_group: string;
  price: string;
}

export interface Shop {
  name: string;
  city: string;
}
