import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
} from '../../auth/token/constants';
import { IDTokenClaims } from '../interfaces/id-token-claims.interface';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(private readonly http: HttpClient) {}

  loadProfile(token, profileUrl) {
    const headers = {
      [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
    };

    return this.http.get<IDTokenClaims>(profileUrl, { headers });
  }
}
